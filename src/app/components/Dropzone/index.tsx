import React from "react";
import styled from "styled-components/macro";
import { useDropzone } from "react-dropzone";

interface Props {
  message: string;
  onDrop: (files: File[]) => void;
}

export function Dropzone({ onDrop, message }: Props) {
  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  return (
    <Wrapper {...getRootProps()}>
      <input {...getInputProps()} />
      <p>{message}</p>
    </Wrapper>
  );
}

Dropzone.defaultProps = {
  onDrop: () => {},
  message: "Drag 'n' drop some files here, or click to select files",
};

const Wrapper = styled.div`
  border: 3px dashed darkgrey;
  padding: 8px;
`;
