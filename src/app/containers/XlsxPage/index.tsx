import React, { useState } from "react";
import styled from "styled-components/macro";
import { Link } from "app/components/Link";
import { NavBar } from "app/containers/NavBar";
import { Helmet } from "react-helmet-async";
import { StyleConstants } from "styles/StyleConstants";
import { Dropzone } from "../../components/Dropzone";
import XLSX, { WorkBook } from "xlsx";

export function XlsxPage() {
  var rABS =
    typeof FileReader !== "undefined" &&
    FileReader.prototype &&
    FileReader.prototype.readAsBinaryString;

  function fixdata(data) {
    var o = "",
      l = 0,
      w = 10240;
    for (; l < data.byteLength / w; ++l)
      o += String.fromCharCode.apply(
        null,
        (new Uint8Array(data.slice(l * w, l * w + w)) as unknown) as number[],
      );
    o += String.fromCharCode.apply(
      null,
      (new Uint8Array(data.slice(o.length)) as unknown) as number[],
    );
    return o;
  }

  function onGetFile(files: File[]) {
    const f = files[0];

    var reader = new FileReader();
    reader.onload = function (e) {
      var data = e?.target?.result ?? {};
      var wb, arr;
      var readtype: { type: "binary" | "base64" } = { type: rABS ? "binary" : "base64" };
      if (!rABS) {
        arr = fixdata(data);
        data = btoa(arr);
      }
      function doit() {
        try {
          wb = XLSX.read(data, readtype);
          process_wb(wb);
        } catch (e) {
          console.log(e);
          // opts.errors.failed(e);
        }
      }

      // @ts-ignore
      if (e?.target?.result?.length > 1e6) {
        console.log("too large");
        doit();
      }
      // opts.errors.large(e.target.result.length, function (e) {
      //   if (e) doit();
      // });
      else {
        doit();
      }
    };
    if (rABS) reader.readAsBinaryString(f);
    else reader.readAsArrayBuffer(f);
  }

  const rowsCount = 1048576;

  function process_wb(workBook: WorkBook) {
    const dataSheet = workBook.Sheets["Данные"];
    if (!dataSheet) {
      console.log("couldn't found the data sheet");
      return;
    }

    let datesCounter = 0;
    for (let i = 1; i < rowsCount; ++i) {
      const cell = dataSheet[`B${i}`];
      const cellContentStr = cell?.w;
      if (cellContentStr && !isNaN(new Date(cellContentStr).getTime())) {
        ++datesCounter;
      }
    }

    setDatesCount(datesCounter);
    console.log(`found ${datesCounter} dates in ${rowsCount} rows`);
  }

  const [datesCount, setDatesCount] = useState<undefined | number>(undefined);

  return (
    <>
      <Helmet>
        <title>Xlsx</title>
        <meta name="description" content="Test xlsx" />
      </Helmet>
      <NavBar />
      <Wrapper>
        <Title>Xlsx Test Page</Title>
        <Dropzone onDrop={onGetFile} />
        {typeof datesCount == "number" && (
          <div>
            Found {datesCount} dates in {rowsCount} rows
          </div>
        )}
        <Link to="/">Return to Home Page</Link>
      </Wrapper>
    </>
  );
}

const Wrapper = styled.div`
  height: calc(100vh - ${StyleConstants.NAV_BAR_HEIGHT});
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  min-height: 320px;
`;

const Title = styled.div`
  margin-top: -8vh;
  font-weight: bold;
  color: ${p => p.theme.text};
  font-size: 3.375rem;

  span {
    font-size: 3.125rem;
  }
`;
